package com.zx.team.view;

import com.zx.team.dao.EmployeeDao;
import com.zx.team.dao.EqipmentDAO;

public class Data {
	 public static final int EMPLOYEE = 1000;
	    public static final int PROGRAMMER =1002;
	    public static final int DESIGNER =1003;
	    public static final int ARCHITECT =1004;
	 
	    public static final int PC =2001;
	    public static final int NOTEBOOK =2002;
	    public static final int PRINTER = 2003;
		static EmployeeDao em=new EmployeeDao();
	    public static final String[][] EMPLOYEES= em.getAllEmployee();
	 
	    //PC      :2001, model, display
	    //NoteBook:2002, model, price
	    //Printer :2003, type, name
		static EqipmentDAO eq=new EqipmentDAO();
	    public static final String[][] EQIPMENTS =eq.getallEquipment();
}
