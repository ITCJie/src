package com.zx.team.service;

import java.util.Scanner;

public class TSUtility {
	private static Scanner scanner = new Scanner(System.in);
	static boolean flag = true;

	public static String readMenuSelection() {

		while (flag) {
			String s = scanner.next();
			if (s.length() != 1) {
				System.out.println("输入错误，请重新输入");
			} else {
				String c = s.substring(1);
				if (s.equals("1") || s.equals("2") || s.equals("3") || s.equals("4")) {
					return s;
				} else
					System.out.println("输入错误，请重新输入");
			}
		}
		return null;

	}

	// 用途：该方法读取键盘，如果用户键入’1’-’4’中的任意字符，则方法返回。返回值为用户键入字符。
	public static void readReturn() throws Exception {
		System.out.print("按回车键继续...");
		readKeyBoard(100, true);
		TeamView team=new TeamView();
		team.enterMainMenu();
	}

	// 用途：该方法提示并等待，直到用户按回车键后返回。
	public static int readInt() {
		int n;
		for (;;) {
			String str = readKeyBoard(2, false);
			try {
				n = Integer.parseInt(str);
				break;
			} catch (NumberFormatException e) {
				System.out.print("数字输入错误，请重新输入：");
			}
		}
		return n;
	}

	private static String readKeyBoard(int limit, boolean blankReturn) {
		String line = "";

		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			if (line.length() == 0) {
				if (blankReturn)
					return line;
				else
					continue;
			}

			if (line.length() < 1 || line.length() > limit) {
				System.out.print("输入长度（不大于" + limit + "）错误，请重新输入：");
				continue;
			}
			break;
		}

		return line;
	}

	// 用途：该方法从键盘读取一个长度不超过2位的整数，并将其作为方法的返回值。
	public static String readConfirmSelection() {
		while (flag) {
			String s = scanner.next();
			if (s.length() != 1) {
				System.out.println("输入错误，请重新输入");
			} else {
				String c = s.substring(1);
				if (s.equals("Y")) {
					return "Y";
				} else if (s.equals("N")) {
					return "N";
				} else {
					System.out.println("输入错误，请重新输入");
				}
			}
		}
		return "N";
	}
	// 用途：从键盘读取‘Y’或’N’，并将其作为方法的返回值。

}
