package com.zx.team.view;

public class Designer extends Programmer{


	public Designer(int id, String name, int age, double salary, Equipment equipment,
			double bonus) {
		super(id, name, age, salary,equipment);
		this.bonus = bonus;
	}

	private double bonus;//����

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	

}
