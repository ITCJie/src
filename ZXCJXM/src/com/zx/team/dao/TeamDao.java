package com.zx.team.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zx.team.DBUTIL.DBUtil;

public class TeamDao {
	public int[] getTeam() {
		Connection coon = DBUtil.getConnection();
		PreparedStatement ps = null;
		try {
			
			String sql = "select * from team";
			ps=coon.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			int[] team= new int[10];
			int i=0;
			while (rs.next()) {
				team[i]=rs.getInt("member_id");
				i++;
			}
			int[] Equipment1 = new int[i];
			for (int j = 0; j < Equipment1.length; j++) {
				Equipment1[j]=team[j];
			}
			return Equipment1;
		} catch (SQLException e) {
			// TODO: handle exception
		}
		return null;


	}
	public void addTeam(int c) {
		Connection coon = DBUtil.getConnection();
		PreparedStatement ps = null;
		try {
			
			String sql = "INSERT INTO team(member_id) VALUES(*)";
			ps=coon.prepareStatement(sql);
			ps.setInt(0, c);
		    ps.execute();
		} catch (SQLException e) {
			// TODO: handle exception
		}

	}
	public void removeTeam(int c) {
		Connection coon = DBUtil.getConnection();
		PreparedStatement ps = null;
		try {
			
			String sql = "delete from team where member_id=*";
			ps=coon.prepareStatement(sql);
			ps.setInt(0, c);
		    ps.execute();
		} catch (SQLException e) {
			// TODO: handle exception
		}

	}

}
