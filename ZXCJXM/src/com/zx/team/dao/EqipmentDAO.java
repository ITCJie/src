package com.zx.team.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zx.team.DBUTIL.DBUtil;

public class EqipmentDAO {
	public String[][] getallEquipment(){
		Connection coon = DBUtil.getConnection();
		PreparedStatement ps = null;
		try {
			
			String sql = "select * from equipment";
			ps=coon.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			String[][] Equipment = new String[10][10];
			int i=0;
			while (rs.next()) {
				Equipment[i][0]=String.valueOf(rs.getInt("id"));
				Equipment[i][1]=rs.getString("eq_number");
				Equipment[i][2]=rs.getString("eq_name");
				i++;
			}
			String[][] Equipment1 = new String[i][3];
			for (int j = 0; j < Equipment1.length; j++) {
				Equipment1[j]=Equipment[j];
			}
			return Equipment1;
			
		} catch (SQLException e) {
			// TODO: handle exception
		}
		return null;

	
	}

}
