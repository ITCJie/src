package com.zx.team.service;

import javax.swing.text.StyleContext.SmallAttributeSet;

import com.zx.team.view.*;
import com.zx.team.view.Data;
import com.zx.team.view.Employee;
import com.zx.team.view.Status;

public class CompanyService {
	private Employee[] employees;

	public Equipment getEquipments(int k) {
		if (k == 2001) {
			Equipment pc = new PC(Data.EQIPMENTS[1][1], Data.EQIPMENTS[1][2]);
			return pc;
		}
		if (k == 2002) {
			Equipment notebook = new NoteBook(Data.EQIPMENTS[1][2], Double.parseDouble(Data.EQIPMENTS[1][1]));
			return notebook;
		}
	
		return null;
	}

	// 方法：获取当前所有员工。返回：包含所有员工对象的数组
	public Employee[] getAllEmployees() {
		employees = new Employee[Data.EMPLOYEES.length];

		for (int i = 0; i < employees.length; i++) {
			if (Integer.parseInt(Data.EMPLOYEES[i][0]) == 1000) {

				employees[i] = new Employee(Integer.parseInt(Data.EMPLOYEES[i][1]), Data.EMPLOYEES[i][2],
						Integer.parseInt(Data.EMPLOYEES[i][3]), Double.parseDouble(Data.EMPLOYEES[i][4]));

			}
			if (Integer.parseInt(Data.EMPLOYEES[i][0]) == 1001) {
				employees[i] = new Programmer(Integer.parseInt(Data.EMPLOYEES[i][1]), Data.EMPLOYEES[i][2],
						Integer.parseInt(Data.EMPLOYEES[i][3]), Double.parseDouble(Data.EMPLOYEES[i][4]),
						getEquipments(Integer.valueOf(Data.EMPLOYEES[i][7])));
				Programmer p = (Programmer) employees[i];
				p.setStatus(Status.FREE);
			}
			if (Integer.parseInt(Data.EMPLOYEES[i][0]) == 1002) {
				employees[i] = new Designer(Integer.parseInt(Data.EMPLOYEES[i][1]), Data.EMPLOYEES[i][2],
						Integer.parseInt(Data.EMPLOYEES[i][3]), Double.parseDouble(Data.EMPLOYEES[i][4]),
						getEquipments(Integer.valueOf(Data.EMPLOYEES[i][7])), Double.parseDouble(Data.EMPLOYEES[i][5]));
				Designer d = (Designer) employees[i];
				d.setStatus(Status.FREE);
			}
			if (Integer.parseInt(Data.EMPLOYEES[i][0]) == 1003) {
				employees[i] = new Architect(Integer.parseInt(Data.EMPLOYEES[i][1]), Data.EMPLOYEES[i][2],
						Integer.parseInt(Data.EMPLOYEES[i][3]), Double.parseDouble(Data.EMPLOYEES[i][4]),
						getEquipments(Integer.valueOf(Data.EMPLOYEES[i][7])), Double.parseDouble(Data.EMPLOYEES[i][6]),
						Integer.parseInt(Data.EMPLOYEES[i][6]));
				Architect d = (Architect) employees[i];
				d.setStatus(Status.FREE);
			}

		}

		return employees;
	}

	// 方法：获取指定ID的员工对象。
	public Employee getEmployee(int id) {
	
		for (Employee employee:this.getAllEmployees()) {
			if (employee.getId() == id)
				return employee;
			
		}
		System.out.println("没有找到该员工");
		return null;
	}
}
