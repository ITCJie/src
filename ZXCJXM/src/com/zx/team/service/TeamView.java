package com.zx.team.service;

import java.sql.SQLException;

import com.zx.team.dao.TeamDao;
import com.zx.team.view.*;

public class TeamView {
	static CompanyService companySvc = new CompanyService();
	static TeamService teamSvc = new TeamService();
	static TSUtility ts = new TSUtility();
	static TeamDao tm=new TeamDao();

	// enterMainMenu ()方法：主界面显示及控制方法。
	// 以下方法仅供enterMainMenu()方法调用：
	// listAllEmployees ()方法：以表格形式列出公司所有成员
	// addMember ()方法：实现添加成员操作
	// deleteMember ()方法：实现删除成员操作
	public void enterMainMenu() throws Exception {
		System.out.println("----------------------------主菜单--------------------------");
		System.out.println("1-团队列表2-添加团队成员3-删除团队成员4-退出 请选择： ");
		String s = ts.readMenuSelection();
		int c = Integer.valueOf(s);
		switch (c) {
		case 1:
			this.listMember();
			break;
		case 2:
			this.addMember();;
			break;
		case 3:
			this.deleteMember();
			break;
		case 4:
			this.exit();
			break;
		}
	}

	private void listAllEmployees() throws Exception {
		System.out.println("-------------------------------公司员工-----------------------------");
		System.out.println("ID\t姓名 \t年龄 \t工资");
		
		Employee employees[]=companySvc.getAllEmployees();
		for (Employee employee : employees) {
			System.out.println(employee.getId()+"\t"+employee.getName()+"\t"+employee.getAge()+"\t"+employee.getSalary());
		}

	}
	//初始化团队
	public void first() throws Exception {
		TeamDao td=new TeamDao();
		int c[]=td.getTeam();
		for (int i : c) {
			Employee employee = companySvc.getEmployee(i);
		    teamSvc.addMember(employee);
		}
	}
	private void listMember() throws Exception {

		Programmer[] team = teamSvc.team;
		System.out.println("------------------------团队成员列表----------------------");
		if(team.length ==0) {
			System.out.println("团队目前还没有成员！");
		}else {
			System.out.println("TID/ID\t姓名\t年龄\t工资\t职位\t奖金\t股票");
			for (Programmer p : team) {
	        	 if (p instanceof Architect) {
	        		 Architect a = (Architect) p;//向下转型，才能访问架构师中的奖金与股票信息
	        		 System.out.println(" " + a.getMemberId() + "/" + a.getId() + "\t"+a.getName()+"\t"+a.getAge()+"\t"+a.getSalary()+"\t架构师\t" +  a.getBonus() + "\t" + a.getStock());
	             } else if (p instanceof Designer) {
	            	 Designer d = (Designer) p;//向下转型，才能访问设计师中的奖金信息
	            	 
	            	 System.out.println(" " +p.getMemberId() + "/" + p.getId() + "\t"+p.getName()+"\t"+p.getAge()+"\t"+p.getSalary()+"\t设计师\t"+((Designer) p).getBonus() );
	             } else if (p instanceof Programmer) {
	            	 System.out.println(" " +p.getMemberId() + "/" + p.getId() + "\t"+p.getName()+"\t"+p.getAge()+"\t"+p.getSalary()+"\t程序员\t");
	             }
	        }
	        System.out.println("-----------------------------------------------------");
	        this.enterMainMenu();
 
		}
		
	}
	private void addMember() throws Exception {
		System.out.println("----------------------------添加成员--------------------------");
		System.out.print("请输入要添加员工的ID：");
			int id = TSUtility.readInt();
			Employee employee = companySvc.getEmployee(id);
			try {
				teamSvc.addMember(employee);
				tm.addTeam(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("删除失败，原因："+e.getMessage());
				System.out.println("----------------------------添加成功--------------------------");
			}
			
		TSUtility.readReturn();
	}

	private void deleteMember() throws Exception {
		System.out.println("----------------------------删除成员--------------------------");
		System.out.print("请输入要删除员工的TID：");
		int id = TSUtility.readInt();
		System.out.print("确认是否删除(Y/N)：");
		String key =TSUtility.readConfirmSelection();
		if (key.equals("N")) return ;
		try {
			teamSvc.removeMember(id);
			tm.removeTeam(id);
			System.out.println("----------------------------删除成功--------------------------");
		} catch (Exception e) {
			System.out.println("删除失败，原因："+e.getMessage());
		}
		TSUtility.readReturn();
	}

	public void exit() throws Exception {
		System.out.print("确认是否要退出?Y/N :");
		String  key = TSUtility.readConfirmSelection();
		if(key.equals("N")){
			TeamView team=new TeamView();
			team.enterMainMenu();
		}
	}
}
