package com.zx.team.view;

public enum Status {
	FREE("空闲",1),BUSY("已经加入开发团队",2),VOCATION("正在休假",3);//FREE-空闲;BUSY-已加入开发团队;VOCATION-正在休假
	private String status;
	private int  id;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private Status(String status, int id) {
		this.status = status;
		this.id = id;
	}

}
