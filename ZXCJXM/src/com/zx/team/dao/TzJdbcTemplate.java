
package com.zx.team.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.RowMapper;

import com.zx.team.DBUTIL.DBUtil;

/**
 * 类描述： jdbc dao模板类 通过参数的动态传入来简化dao类的代码 1：sql 2：sql需要传入的变量的值
 * 类名称：com.tz.framework.db.util.TzJdbcTemplate
 * 
 * @author 南天
 * @date 2017年4月15日
 * @version 1.0
 */

public class TzJdbcTemplate {

	
	/**
	 * 封装增删改的模板方法
	 */
	public int updateTemplate(String sql,Object[] params){
		int rows = 0;
		Connection con = null; //一个数据库连接
		PreparedStatement pre = null; //预编译对象
		ResultSet result = null; //结果集
		try{
			con = DBUtil.getConnection();
			pre = con.prepareStatement(sql);
			//设置sql所需要的参数
			setParams(params, pre);
			rows = pre.executeUpdate();			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DBUtil.free(con, pre, result);
		}
		return rows;
	}
	
	/**
	 * 查询的封装方法
	 */
	public <T> List<T> queryForList(RowMapper mapper,String sql,Object[] params){
		List<T> returnResult = new ArrayList<T>();
 		Connection con = null; //一个数据库连接
		PreparedStatement pre = null; //预编译对象
		ResultSet result = null; //结果集
		try{
			con = DBUtil.getConnection();
			pre = con.prepareStatement(sql);
			//设置sql所需要的参数
			setParams(params, pre);
			result = pre.executeQuery();
			int rownum = 0;
			while(result.next()){
				rownum++;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DBUtil.free(con, pre, result);
		}
		return returnResult;
	}
	/**
	 * 动态设置参数
	 */
	private void setParams(Object[] params, PreparedStatement pre) throws SQLException {
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				pre.setObject(i + 1, params[i]);
			}
		}
	}

}
