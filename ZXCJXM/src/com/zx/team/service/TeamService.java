package com.zx.team.service;

import com.zx.team.view.Architect;
import com.zx.team.view.Data;
import com.zx.team.view.Designer;
import com.zx.team.view.Employee;
import com.zx.team.view.Programmer;
import com.zx.team.view.Status;

public class TeamService {
//	counter为静态变量，用来为开发团队新增成员自动生成团队中的唯一ID，即memberId。（提示：应使用增1的方式）
//	MAX_MEMBER表示开发团队最大成员数
//	team数组用来保存当前团队中的各成员对象 
//	realCount记录团队成员的实际人数
//	getTeam()方法：返回当前团队的所有有效对象
//	返回：包含所有成员对象的数组，数组大小与成员人数一致
//	addMember(e: Employee)方法：向团队中添加成员
	private static int counter;
	final private int MAX_MEMBER =6;//开发团队最大成员数
	private int total =0;//记录团队成员的实际人数
	private int counterPro = 0,counterAch = 0,counterDes = 0;
	Programmer[] team = new Programmer[MAX_MEMBER];
	public Programmer[] getTeam(){
			Programmer[] team = new Programmer[total];
			for (int i = 0; i <total; i++) {
				team[i] = this.team[i];
			}
			return team;
	}
	public void addMember(Employee e) throws Exception {
		if (total >= MAX_MEMBER) {
			throw new Exception("团队成员已满，无法继续添加！");
		}
		if (!(e instanceof Programmer)) {
			throw new Exception("该成员不是开发人员，无法添加！");
		}
		
		Programmer p = (Programmer) e;
	
		switch (p.getStatus()) {
			case BUSY:
				throw new Exception("该成员正忙！");
			case VOCATION:
				throw new Exception("该成员正在休假中！");
		}
		if (isTrue(p)) {
			throw new Exception("该员工已是团队成员！");
		}
		
		if (p instanceof Architect) {
			if (counterAch>=1) {
				throw new Exception("团队中只能有一名架构师！");
			}
		}
		if (p instanceof Designer) {
			if (counterAch>=2) {
				throw new Exception("团队中只能有两名设计师！");
			}
		}
		if (p instanceof Programmer) {
			if (counterAch>=3) {
				throw new Exception("团队中只能有三名程序员！");
			}
		}
		p.setStatus(Status.BUSY);
		p.setMemberId(counter++);
		team[total] = p;
		total++;
		
		if (p instanceof Architect) {
			counterAch++;
		}else if (p instanceof Designer) {
			counterDes++;
		}else if (p instanceof Programmer) {
			counterPro++;
		}

	}
	public boolean isTrue(Programmer p) {
		for (int i = 0; i < total; i++) {
			if (p.getId() == team[i].getId()) 
				return true;
		}
		return false;
	}
	public void removeMember(int id) throws Exception {
		// TODO Auto-generated method stub
		 int n = 0;//记录要删除的员工的下标
	        for (; n < total; n++) {
	            if (team[n].getId() == id) {
	                team[n].setStatus(Status.FREE);
	                break;
	            }
	        }
	      if (n==total) 
			throw new Exception("改成员无法删除");
	      for (int i = n + 1; i < total; i++) {
	            team[i - 1] = team[i];//后一个元素复制给前一个元素
	        }
	        team[--total] = null;//总人数减1，最后一个元素因为已经复制到前一个元素中了，因此置为null
	}
}
